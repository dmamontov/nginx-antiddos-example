FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY otus.txt  /opt/otus.txt
COPY index2.html  /mnt/index2.html
CMD ["nginx","-g","daemon off;"]
